---
layout: markdown_page
title: "FY22 GTM Sales Play - Premium to Ultimate"
---

## Big picture

**Demand generation**
1. placeholder

**Activities and support**
1. placeholder


## Context (campaign promise/offer) 

Objective to convert landed accounts that are already using CI/CD to expand from premium to ultimate. This play is FYI only for SDRs because it is upselling tiers, not necessarily expanding seats. 

Related sales enablement content: 
* [SKO Expanding to Ultimate](https://docs.google.com/presentation/d/1oq7ODy9TJpuZqH_tvVtCm2t-C0QkTbuG4ZRlRzRNcUY/edit#slide=id.gb4749ff26b_0_85)
* [Selling security]() see John Blevin's online classroom material
* [Selling DevSecOps](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/#ultimate)

Marketing is targeting a campaign at [these accounts](https://gitlab.my.salesforce.com/00O4M000004akG8) (as of 2021-03-22) XYZ persona will receive [this email]() inviting them to [zzz web page]()  (or collateral or offer). (See [this epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1901) for details.)

Emphasis on large due to the relation to verticals + regulated industries, but all should be included.

**Key messages used in the campaign**

* ENT: GitLab unifies teams together across the entire SDLC regardless of role - product managers, designers, developers, managers, and all roles in between, can work more efficiently and stay involved as a part of a single conversation across the SDLC. Helps enterprises create products relevant for customers, go to market faster and beat the competition while staying secure and compliant. 
* MM: Improve efficiencies, collaboration, and eliminate bottlenecks. Spend time on planned work instead of maintaining and integrating tools. Get expertise and support that assures you can meet your strategic goals along the way
* SMB: Set yourself up for long term success and keep your developers happy - code reviews, source control, continuous integration, and continuous deployment all tied together and all roles can speak the same language. Start right before inefficiencies get in the way of scaling out successfully
* PubSec:  Manage risk from security and regulatory compliance by identifying vulnerabilities and compliance issues before the code ever leaves the developer's hands. Stop worrying about audits!

**Why sell this?**
* Ultimate drives large deals and is very sticky
* Security adds seats
* TAM is xxx
* Contributes toward revenue goals of xxx 
* Partners: grow new markets, new business, cross-sell, upsell opportunity, partner profitability, competitive advantage, compliment with services.

## Who to meet with 

**Economic buyers:**  
* CISO or Security Manager, VP of Security, Director of Security, VP of IT or CTO, App/Dev Director

**Technical influencers:**  
* Chief Architect, App Dev Manager

**Other Personas to consider:**   
* Infrastructure Engineering Director, Release and Change Management Director 



## What to say 

**Elevator pitch** 
Connect to campaign messages above. GitLab Ultimate is ideal for projects with executive visibility and strategic organizational usage. Ultimate enables IT transformation by optimizing and accelerating delivery while managing priorities, security, risk, and compliance. 

Everything you get in Premium as well as free guest users, 50,000 CI/CD minutes, and more…
GitLab Ultimate provides the single tool DevOps teams need to find and fix vulnerabilities in application code and its environments and to manage their risk from detection through remediation


**Ask these questions:**

1. Happy with Jira? 
   * No - talk about GitLab Plan Plan pitch
1. Wanting to shift security left in DevOps/Agile environment?
   * Yes - Lead with integrated platform to simplify
   * No - If security says so, verify with dev and use security pitch deck
1. Incumbent tools provide insight into psych 
   * If Snyk or Checkmarx, continue with integrated platform
   * If Veracode, Fortify, Synopsis, assess priorities to reduce vs replace 
1. Using containers & Kubernetes?
   * Yes - Lead with integrated platform to modernize software dev

**Objection handling**
- [Potential objections and how to respond](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/#potential-objections)

#### [Message house](/handbook/marketing/plan-fy22/gtm-cicd/premium-to-ultimate-planner/fy22-gtm-premium-to-ultimate-playbook/message-house)

## What to show/info to use

Use **these slides** 
* Plan pitch deck 
* [Security pitch deck for shift left](https://docs.google.com/presentation/d/1iH7DbyDMX9kvTWUz2rh0Da9xPTHqZdNfGY70DfiAMPY/edit?usp=sharing) 
* [Security CISO deck](https://docs.google.com/presentation/d/1toM1YIkIjLAmXMPPT3XCS-tryA8pKef6wnGe-DDp148/edit?usp=sharing) (includes ROI framework slide)

Use **this collateral**:  

1. Gartner MQ for AST expected in May 2021
1. New Gartner MQ for Plan
1. Why Premium to Ultimate?
1. placeholder


And these **customer stories**: (reevaluate if these are still the best ones for Plan & Secure)
* ~20 tools consolidated into GitLab; Glympse remediated security issues faster than any other company in their Security Auditor's experience

Development can move much faster when engineers can stay on one page and click buttons to release auditable changes to production and have easy rollbacks; everything is much more streamlined. Within one sprint, just 2 weeks, Glympse was able to implement security jobs across all of their repositories using GitLab’s CI templates and their pre-existing Docker-based deployment scripts.
Zaq Wiedmann
Lead Software Engineer, Glympse
[Read more](https://about.gitlab.com/customers/glympse/)

* Jenkins build took 3 hours, now with GitLab it takes 30 mins: a 6x improvement

GitLab has allowed Alteryx to have code reviews, source control, continuous integration, and continuous deployment all tied together and speaking the same language. The team took a build that was running legacy systems and moved it to GitLab. This build took 3 hours on the Jenkins machine and it took 30 minutes to run on GitLab after it was going. Engineers can actually look at the build and understand what’s going on; they’re able to debug it and make it successful
[Read more](https://about.gitlab.com/customers/alteryx/)

* BI Worldwide performed 300 SAST/Dependency scans in the first 4 days of use helping the team identify previously unidentified vulnerabilities

One tool for SCM+CI/CD was a big initial win. Now wrapping security scans into that tool as well has already increased our visibility into security vulnerabilities. The integrated Docker registry has also been very helpful for us. Issue/Product management features let everyone operate in the same space regardless of role.
Adam Dehnel
Product architect, BI Worldwide
[Read more](https://about.gitlab.com/customers/bi_worldwide/)

Use this **POV boiler plate**: xxx (SA team to provide)

**Note: May want to move this information to slides:** 

Key capabilities in Ultimate center around security, compliance, insights/analytics, and releasing better + faster: 

[Ultimate specific features](https://about.gitlab.com/pricing/ultimate/#ultimate-specific-features)

Ultimate also includes priority support (4 business hour support), live upgrade assistance, and a Technical Account Manager who will work with you to achieve your strategic objectives and gain maximum value from your investment in GitLab.

**Differentiators** (move to slides) 
How does GitLab uniquely deliver this business outcome vs. competitors? 

Truly shift security left (and right!) to reduce exposure and align security with dev. Emergent advantages include unparalleled visibility and insights and much easier traceability with consistent compliance to policies or regulatory requirements for cleaner, easier audits. 
1. **Single application with end-to-end visibility and insights**
   1. From developers to managers to execs. Lovable developer UX with executive visibility. No one asks “what changed?”
   1. Reduces exposure and unifies teams- with emergent advantages including unparalleled visibility and insights and much easier traceability.
1. **Embedded security that’s contextual and congruent to DevOps processes**
   1. Built in security and compliance shifted all the way left (and all the way right) - reduces risk and scales
   1. Consistent compliance to policy, for cleaner and easier audits 
1. **Leading SCM, CI, and code review in one**
   1. Completely integrated from idea to production
   1. No plugins, etc.
1. **Transparent and collaborative**
   1. Single source of truth with built-in collaboration to focus on remediation, less friction
   1. Auditable - see who changed what where, when including policy exceptions.


## How to measure progress

- [ ] Gameplan with GitLab champion
- [ ] Meeting with Security team or other economic buyer
- [ ] Agreement to do POV

Note: progress of the GTM Motion will be measured at the campaign level with clicks/opens/page visits, SAO (is there a code sales needs to use in SFDC?)

