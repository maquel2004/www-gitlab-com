---
layout: handbook-page-toc
title: "SG.2.01 - Information Security Program Content Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# SG.2.01 - Information Security Program Content

## Control Statement

The GitLab Security Department Leadership conducts a monthly staff meeting to communicate and align on relevant security threats, program performance, and resource prioritization.

## Context

By holding meetings to communicate information about the security program and relevant security threats, GitLab team-members can better understand GitLab's overall security posture, future initiatives, and the threat landscape. Such meetings also afford an opportunity to engage with and learn more about security, the benefits of which can extend outside the security department and bring value to customers and partners.

## Scope

TBD

## Ownership

GitLab's Director of Security

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Information Security Program Content control issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/issues/877).

### Policy Reference

## Framework Mapping

* SOC2 CC
  * CC3.2
* PCI
  * SAQ-A
