---
layout: handbook-page-toc
title: "Identity data"
description: "GitLab country specific data in regard to team members location, gender, ethnicity, race, age etc. View data here!"
canonical_path: "/company/culture/inclusion/identity-data/"
---

#### GitLab Identity Data

Data as of 2021-03-31

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 147       | 11.43%          |
| Based in EMEA                               | 358       | 27.84%          |
| Based in LATAM                              | 23        | 1.79%           |
| Based in NORAM                              | 758       | 58.94%          |
| **Total Team Members**                      | **1,286** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 876       | 68.12%          |
| Women                                       | 410       | 31.88%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **1,286** | **100%**        |

| **Gender in Management**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Management                           | 118       | 62.43%          |
| Women in Management                         | 71        | 37.57%          |
| Other Gender Management                     | 0         | 0%              |
| **Total Team Members**                      | **189**   | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 75        | 72.82%          |
| Women in Leadership                         | 28        | 27.18%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **103**   | **100%**        |

| **Gender in Engineering**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Engineering                          | 440       | 79.71%          |
| Women in Engineering                        | 112       | 20.29%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **552**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.14%           |
| Asian                                       | 68        | 9.63%           |
| Black or African American                   | 23        | 3.26%           |
| Hispanic or Latino                          | 39        | 5.52%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 34        | 4.82%           |
| White                                       | 536       | 75.92%          |
| Unreported                                  | 5         | 0.71%           |
| **Total Team Members**                      | **706**   | **100%**        |

| **Race/Ethnicity in Engineering (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 21        | 10.45%          |
| Black or African American                   | 3         | 1.49%           |
| Hispanic or Latino                          | 6         | 2.99%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 10        | 4.98%           |
| White                                       | 160       | 79.60%          |
| Unreported                                  | 1         | 0.50%           |
| **Total Team Members**                      | **201**   | **100%**        |

| **Race/Ethnicity in Management (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 11        | 8.94%           |
| Black or African American                   | 4         | 3.25%           |
| Hispanic or Latino                          | 4         | 3.25%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 8         | 6.50%           |
| White                                       | 96        | 78.05%          |
| Unreported                                  | 0         | 0%              |
| **Total Team Members**                      | **123**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 13        | 15.85%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 3         | 3.66%           |
| White                                       | 66        | 80.49%          |
| Unreported                                  | 0         | 0%              |
| **Total Team Members**                      | **82**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.08%           |
| Asian                                       | 152       | 11.82%          |
| Black or African American                   | 36        | 2.80%           |
| Hispanic or Latino                          | 71        | 5.52%           |
| Native Hawaiian or Other Pacific Islander   | 2         | 0.16%           |
| Two or More Races                           | 43        | 3.34%           |
| White                                       | 821       | 63.84%          |
| Unreported                                  | 160       | 12.44%          |
| **Total Team Members**                      | **1,286** | **100%**        |

| **Race/Ethnicity in Engineering (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 68        | 12.32%          |
| Black or African American                   | 11        | 1.99%           |
| Hispanic or Latino                          | 28        | 5.07%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.18%           |
| Two or More Races                           | 16        | 2.90%           |
| White                                       | 335       | 60.69%          |
| Unreported                                  | 92        | 16.85%          |
| **Total Team Members**                      | **552**   | **100%**        |

| **Race/Ethnicity in Management (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 18        | 9.52%           |
| Black or African American                   | 4         | 2.12%           |
| Hispanic or Latino                          | 6         | 3.17%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 9         | 4.76%           |
| White                                       | 136       | 71.96%          |
| Unreported                                  | 16        | 8.47%           |
| **Total Team Members**                      | **189**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 14        | 13.59%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 0.97%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 3         | 2.91%           |
| White                                       | 77        | 74.76%          |
| Unreported                                  | 8         | 7.77%           |
| **Total Team Members**                      | **103**   | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 15        | 1.17%           |
| 25-29                                       | 197       | 15.32%          |
| 30-34                                       | 361       | 28.07%          |
| 35-39                                       | 302       | 23.48%          |
| 40-49                                       | 275       | 21.38%          |
| 50-59                                       | 117       | 9.10%           |
| 60+                                         | 19        | 1.48%           |
| Unreported                                  | 0         | 0.00%           |
| **Total Team Members**                      | **1,286** | **100%**        |

**Of Note**: `Management` refers to Team Members who are *People Managers*, whereas `Leadership` denotes Team Members who are in *Director-level positions and above*.

**Source**: GitLab's HRIS, BambooHR
